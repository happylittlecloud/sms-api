#!/usr/bin/env python3

# Send the message to my number

from twilio.rest import Client
from os import getenv
from fastapi import FastAPI
from pydantic import BaseModel

MY_PHONE = getenv('PHONE')

TWILIO_SMS        = getenv('TWILIO_SMS')
TWILIO_SID        = getenv('TWILIO_SID')
TWILIO_AUTH_TOKEN = getenv('TWILIO_AUTH_TOKEN')

# the following line needs your Twilio Account SID and Auth Token
client = Client(TWILIO_SID, TWILIO_AUTH_TOKEN)

# change the "from_" number to your Twilio number and the "to" number
# to the phone number you signed up for Twilio with, or upgrade your
# account to send SMS to any phone number

app = FastAPI(debug=True)

class Message(BaseModel):
    sender: str
    message: str

@app.get("/")
def hello():
    return "hello world"

@app.post("/sendsms")
def sendsms(m: Message):
    client.messages.create(
        to=MY_PHONE,
        from_=TWILIO_SMS,
        body='@\n{}\n{}\n{}'.format( m.sender, '='*len(m.sender), m.message )
    )
    return "Message sent."
